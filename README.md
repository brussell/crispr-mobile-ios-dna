# DNA

This library is intended to work for macOS Catalyst, watchOS, tvOS, iPadOS, and iOS. It provides helpful resuable components as well as ablity to theme the app with sets of colors and fonts.

To publish an update:
After new code is merged in with a pull request, tag that commit with the version number

Using the apps:
The Demo and Demo-SwiftUI apps show some best practices using this Swift pacakage as well as some helpful reusable parts such as ConfigController and NetworkController.
ConfigController is a great starter for a server and/or app bundled config file that specifies some configuration settings.
NetworkController is useful for a starter that supports authentication and retrying requests after authentication is refreshed, if needed. It also shows how to mock network requests in order to do unit testing in other areas that don't not require networking.
Some of these classes may potentially be brought into the Swift package directly if it is deemed useful to override rather than modify.

Using the Swft Package:
Add Swift Package Dependency to repo `https://bitbucket.org/mediciventures/crispr-mobile-ios-dna.` 
