//
//  UserDefaultsTests.swift
//  DemoTests
//
//  Created by Branden Russell on 10/28/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

import XCTest
@testable import DNA

class UserDefaultsTests: XCTestCase {

    func testClearAll() {
        UserDefaults.standard.set("Test", forKey: "Test")
        UserDefaults.standard.synchronize()
        XCTAssertEqual(UserDefaults.standard.string(forKey: "Test"), "Test", "Value didn't save")
        UserDefaults.standard.clearAll()
        XCTAssertNil(UserDefaults.standard.string(forKey: "Test"), "Clear all didn't work")
    }

}
