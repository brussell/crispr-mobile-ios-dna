//
//  BarButtonItemWithTapHandlerTests.swift
//  DemoTests
//
//  Created by Branden Russell on 10/31/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

import XCTest
@testable import DNA

#if canImport(UIKit)
class BarButtonItemWithTapHandlerTests: XCTestCase {
    
    func testTapped() {
        let expectation = XCTestExpectation(description: "Bar Button Item Tapped")
        let barButtonItem = BarButtonItemWithTapHandler(barButtonSystemItem: .done, target: nil, action: nil)
        barButtonItem.configure { _ in
            expectation.fulfill()
        }
        barButtonItem.tapped()
        wait(for: [expectation], timeout: 1)
    }
    
}
#endif
