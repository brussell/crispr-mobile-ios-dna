//
//  UIColorTests.swift
//  DemoTests
//
//  Created by Branden Russell on 10/31/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

import XCTest
@testable import DNA

#if canImport(UIKit)
class UIColorTests: XCTestCase {

    func testLighterAndDarker() {
        let background = UIColor.color(for: "Background", in: ThemeType.default.displayName)
        let darkerBackground = background.darker
        let lighterBackground = background.lighter
        XCTAssertNotEqual(background, lighterBackground, "Colors didn't lighten/darken")
        XCTAssertNotEqual(darkerBackground, lighterBackground, "Colors didn't lighten/darken")
    }
    
}
#endif
