//
//  UITextFieldTests.swift
//  DemoTests
//
//  Created by Branden Russell on 10/31/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

import XCTest
@testable import DNA

#if canImport(UIKit)
class UITextFieldTests: XCTestCase {
    
    func testPlaceholderColorWithPlaceholder() {
        let textField = UITextField()
        textField.placeholder = "Test"
        textField.placeholderColor = .green
        XCTAssertEqual(textField.placeholderColor, UIColor.green, "Placeholder color didn't get set")
        XCTAssertEqual(textField.placeholder, "Test")
        textField.placeholderColor = nil
        XCTAssertNil(textField.placeholderColor, "Placeholder color didn't nil")
    }
    
    func testAddAccessoryView() {
        let textField = UITextField()
        textField.addDoneButtonAccessoryView(tapHandler: {_ in})
        XCTAssertEqual((textField.inputAccessoryView as! UIToolbar).items!.count, 2, "Done button wasn't created")
        textField.addNextButtonAccessoryView(tapHandler: {_ in})
        XCTAssertEqual((textField.inputAccessoryView as! UIToolbar).items!.count, 2, "Next button wasn't created")
    }
    
    func testNextField() {
        let textField = UITextField()
        let textField2 = UITextField()
        textField.nextField = textField2
        XCTAssertEqual(textField.nextField, textField2)
    }
    
}
#endif
