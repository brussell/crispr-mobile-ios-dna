//
//  NumberTests.swift
//  DemoTests
//
//  Created by Branden Russell on 10/28/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

import XCTest
@testable import DNA

class NumberTests: XCTestCase {

    func testDigits() {
        XCTAssertEqual("1234".digits, [1, 2, 3, 4], "String didn't convert to digits")
        XCTAssertEqual(1234.digits, [1, 2, 3, 4], "Int didn't convert to digits")
        XCTAssertEqual(Double(1234).digits, [1, 2, 3, 4, 0], "Double didn't convert to digits")
    }

}
