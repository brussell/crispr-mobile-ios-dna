//
//  SubtitleTableViewCellTests.swift
//  DemoTests
//
//  Created by Branden Russell on 10/31/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

import XCTest
@testable import DNA

#if canImport(UIKit)
class SubtitleTableViewCellTests: XCTestCase {

    func testLabels() {
        let cell = SubtitleTableViewCell()
        cell.descriptionLabel.text = "Title"
        cell.subtitleLabel.text = "Subtitle"
        XCTAssertEqual(cell.descriptionLabel.text, cell.textLabel?.text, "textLabel wasn't set correctly")
        XCTAssertEqual(cell.subtitleLabel.text, cell.detailTextLabel?.text, "detailTextLabel wasn't set correctly")
        XCTAssertEqual(cell.accessibilityLabel, "\(cell.descriptionLabel.text!), \(cell.subtitleLabel.text!)", "Accessibility label wasn't set correctly")
        cell.subtitleLabel.text = nil
        XCTAssertEqual(cell.accessibilityLabel, cell.descriptionLabel.text, "Accessibility label wasn't set correctly")
        cell.accessibilityLabel = "Custom"
        XCTAssertEqual(cell.accessibilityLabel, "Custom", "Accessibility label wasn't set correctly")
    }
    
    func testLabelsAfterOverwritting() {
        let cell = SubtitleTableViewCell()
        cell.descriptionLabel.text = "Title"
        cell.subtitleLabel.text = "Subtitle"
        XCTAssertEqual(cell.descriptionLabel.text, "Title", "descriptionLabel wasn't set correctly")
        XCTAssertEqual(cell.subtitleLabel.text, "Subtitle", "subtitleLabel wasn't set correctly")
        
        let newTextLabel = UILabel()
        newTextLabel.text = "NewTitle"
        cell.textLabel = newTextLabel
        
        let newDetailTextLabel = UILabel()
        newDetailTextLabel.text = "NewSubtitle"
        cell.detailTextLabel = newDetailTextLabel
        
        XCTAssertEqual(cell.descriptionLabel.text, "NewTitle", "descriptionLabel wasn't updated correctly")
        XCTAssertEqual(cell.subtitleLabel.text, "NewSubtitle", "subtitleLabel wasn't updated correctly")
    }
    
}
#endif
