//
//  StringTests.swift
//  DemoTests
//
//  Created by Branden Russell on 10/28/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

import XCTest
@testable import DNA

class StringTests: XCTestCase {

    func testTrimmed() {
        XCTAssertEqual("test".trimmed, "test")
        XCTAssertEqual("test ".trimmed, "test")
        XCTAssertEqual(" test\n ".trimmed, "test")
        XCTAssertEqual(" test".trimmed, "test")
        XCTAssertEqual("\n test".trimmed, "test")
        XCTAssertEqual(" \n test \n\n".trimmed, "test")
    }
    
    func testIsNilOrEmpty() {
        let testNil: String? = nil
        XCTAssertTrue(testNil.isNilOrEmpty)
        let testEmpty: String? = ""
        XCTAssertTrue(testEmpty.isNilOrEmpty)
        let testNotEmpty: String? = "Stuff"
        XCTAssertFalse(testNotEmpty.isNilOrEmpty)
    }
    
}
