//
//  FileManagerTests.swift
//  DemoTests
//
//  Created by Branden Russell on 10/28/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

import XCTest
@testable import DNA

final class FileManagerTests: XCTestCase {

    func testDirectoriesExist() {
        XCTAssertNotNil(FileManager.default.applicationLibraryDirectory, "applicationLibraryDirectory not found")
        XCTAssertNotNil(FileManager.default.applicationDocumentsDirectory, "applicationDocumentsDirectory not found")
        XCTAssertNotNil(FileManager.default.downloadsDirectory, "downloadsDirectory not found")
    }

}
