//
//  ArrayTests.swift
//  DemoTests
//
//  Created by Branden Russell on 10/28/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

import XCTest
@testable import DNA

final class ArrayTests: XCTestCase {
    
    func testMissingIsNil() {
        let array = [1, 2, 3]
        XCTAssertNil(array[safe: 10])
        XCTAssertNil(array[safe: 4])
        XCTAssertNil(array[safe: -1])
    }
    
    func testCorrectValue() {
        let array = [0, 1, 2]
        Array(0..<array.count).forEach { index in
            XCTAssertTrue(array[safe: index] == index)
        }
    }
    
    func testDontCrashSetOutOfRange() {
        var array = [1, 2, 3]
        array[safe: 5] = 10
        array[safe: -1] = 10
        array[safe: 10] = 10
        XCTAssertEqual([1, 2, 3], array)
    }
    
    func testSetInRange() {
        var array = [1, 2, 3]
        array[safe: 0] = 4
        array[safe: 1] = 5
        array[safe: 2] = 6
        XCTAssertEqual([4, 5, 6], array)
    }

}
