import XCTest

import DNATests

var tests = [XCTestCaseEntry]()
tests += DNATests.allTests()
XCTMain(tests)
