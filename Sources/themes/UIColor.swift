//
//  UIColor.swift
//  CRISPR
//
//  Created by Branden Russell on 10/28/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

#if canImport(UIKit)
import UIKit

public extension UIColor {
    
    /// Lightens the color 20%
    var lighter: UIColor {
        var hue: CGFloat = 0
        var saturation: CGFloat = 0
        var brightness: CGFloat = 0
        var alpha: CGFloat = 0
        
        guard getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha) else { return self }
        
        return UIColor(hue: hue, saturation: saturation, brightness: min(1.0, brightness * 1.2), alpha: alpha)
    }
    
    /// Darkens the color 20%
    var darker: UIColor {
        var hue: CGFloat = 0
        var saturation: CGFloat = 0
        var brightness: CGFloat = 0
        var alpha: CGFloat = 0
        
        guard getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha) else { return self }
        
        return UIColor(hue: hue, saturation: saturation, brightness: max(0.0, brightness * 0.8), alpha: alpha)
    }
     
    /// Gets the color for the specified property in the theme. If that property isn't in that theme, it gets it from the default theme
    static func color(for property: String, in themeName: String) -> UIColor {
        let mainUIColor = UIColor(named: "\(themeName)-\(property)") ?? UIColor(named: "\(ThemeType.default.displayName)-\(property)")
        
        // watchOS can't get colors from a specified bundle and the colors must be specified in the watchOS Extensions's Assets.xcassets
        #if os(watchOS)
            return mainUIColor!
        #else
            return mainUIColor ?? UIColor(named: "\(themeName)-\(property)", in: Bundle.theme, compatibleWith: nil) ?? UIColor(named: "\(ThemeType.default.displayName)-\(property)", in: Bundle.theme, compatibleWith: nil)!
        #endif
    }
    
}

#endif
