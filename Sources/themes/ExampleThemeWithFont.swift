//
//  ExampleThemeWithFont.swift
//  CRISPR
//
//  Created by Branden Russell on 8/14/20.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

#if canImport(UIKit)
import UIKit

/// Example of custom Theme that adds custom fonts
public class ExampleThemeWithFont: Theme {
    /// Here just to make it so this can be initialized outside the library, if neede
    public override init() {
    }
    
    /// This is showing how you can use the same named themes as default, but customize the font used
    /// - Parameter themeName: Name of theme. Provided by ThemeType.displayName
    public override func setTheme(with themeName: String) {
        // Just get all the colors and default settings from the base
        super.setTheme(with: themeName)
        
        // This could have a switch on the name to do something specific with the keyboard appearance or to reuse colors without having to duplicate them in xcassets
        // In this case, we're doing an example custom font
        switch themeName {
        case ThemeType.blue.displayName:
            #if os(iOS)
            customFont = UIFont(name: "ChalkboardSE-Regular", size: UIFont.labelFontSize)
            #else
            customFont = UIFont(name: "ChalkboardSE-Regular", size: 17)
            #endif
        case _:
            #if os(iOS)
            customFont = UIFont(name: "Avenir-Medium", size: UIFont.labelFontSize)
            #else
            customFont = UIFont(name: "Avenir-Medium", size: 17)
            #endif
        }
    }
}
#endif
