//
//  ThemeController.swift
//  CRISPR
//
//  Created by Branden Russell on 7/3/19.
//  Copyright © 2020 Chainstone. All rights reserved.
//

#if canImport(UIKit)
import UIKit
import Combine

public class ThemeController: ObservableObject {

    public static var shared = ThemeController()
    
    public init() {
        themeName = UserDefaults.standard.string(forKey: "themeName") ?? ThemeType.default.displayName
        
        #if !os(watchOS)
        // SwiftUI doesn't currently support custom fonts that automatically change with accessibility. Listen for the change to trigger an update so we can act like SwiftUI does support this
        NotificationCenter.default.addObserver(self, selector: #selector(contentSizeCategoryDidChange), name: UIContentSizeCategory.didChangeNotification, object: nil)
        #endif
    }
    
    /// This is a referencable version of whatever the theme is set to. Changing this will trigger the UI to update colors/fonts/styles
    @Published public var theme: Theme = Theme() {
       willSet {
           newValue.setTheme(with: themeName)
       }
    }
    
    /// Get/Set the theme name the app currently uses. Setting will notify the observers to have them update to new theme
    @Published public var themeName = ThemeType.default.displayName {
        willSet {
            UserDefaults.standard.set(newValue, forKey: "themeName")
            theme.setTheme(with: newValue)
            
            updateAppearance()
        }
    }
    
    /// Called when the accessibility changes so SwiftUI can be triggered to refresh the views
    @objc func contentSizeCategoryDidChange() {
        // SwiftUI doesn't currently support custom fonts that automatically change with accessibility. We have to force this @Published property to think it changed to trigger the Views to update
        self.theme = ThemeController.shared.theme
    }
    
    /// Sets all the colors on needed Appearance classes
    public func updateAppearance() {
        #if !os(watchOS)
        UIApplication.shared.windows.forEach { window in
            window.tintColor = theme.accentColor
            // This is needed or tvOS won't color the whole background. Might be fine to remove the if and let all platforms do this
            #if os(tvOS)
            window.backgroundColor = theme.backgroundColor
            #endif
        }

        UINavigationBar.appearance().tintColor = theme.navigationBarAccentColor
        UINavigationBar.appearance().barTintColor = theme.navigationBarBackgroundColor

        UITableView.appearance().backgroundColor = theme.listBackgroundColor
        // To remove only extra separators below the list
        UITableView.appearance().tableFooterView = UIView()
        UITableViewCell.appearance().backgroundColor = theme.listItemBackgroundColor

        let selectedView = UIView()
        selectedView.backgroundColor = theme.listItemSelectedColor
        UITableViewCell.appearance().selectedBackgroundView = selectedView
        UICollectionView.appearance().backgroundColor = theme.listBackgroundColor
        UICollectionViewCell.appearance().backgroundColor = theme.listItemBackgroundColor
        #endif

        #if os(iOS)
        updateAppearanceiOS()
        #endif
    }
    
    #if os(iOS)
    /// Parts of appearance update that aren't supported on tvOS at the moment
    private func updateAppearanceiOS() {
        UITableView.appearance().separatorColor = theme.listSeparatorColor
        
        UIScrollView.appearance().backgroundColor = theme.backgroundColor
        
        let largeAppearance = UINavigationBarAppearance()
        // This one doesn't have the seperator line, if design wants that removed
        largeAppearance.configureWithTransparentBackground()
        largeAppearance.backgroundColor = theme.backgroundColor
        largeAppearance.largeTitleTextAttributes = [
            NSAttributedString.Key.foregroundColor: theme.navigationBarLargeTitleColor,
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20)
        ]
        UINavigationBar.appearance().scrollEdgeAppearance = largeAppearance
        
        let inlineAppearance = UINavigationBarAppearance()
        inlineAppearance.configureWithOpaqueBackground()
        inlineAppearance.backgroundColor = theme.navigationBarBackgroundColor
        inlineAppearance.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: theme.navigationBarTitleColor
        ]
        UINavigationBar.appearance().standardAppearance = inlineAppearance
        
        let standardAppearance = UITabBarAppearance()
        standardAppearance.configureWithOpaqueBackground()
        standardAppearance.backgroundColor = theme.toolbarBackgroundColor
        setTabBarItemColors(standardAppearance.stackedLayoutAppearance)
        setTabBarItemColors(standardAppearance.inlineLayoutAppearance)
        setTabBarItemColors(standardAppearance.compactInlineLayoutAppearance)
        UITabBar.appearance().standardAppearance = standardAppearance
        
        UISwitch.appearance().onTintColor = theme.accentColor
        
        UISegmentedControl.appearance().selectedSegmentTintColor = theme.segmentedControlSelectedBackgroundColor
        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor: theme.segmentedControlSelectedTextColor], for: .selected)
        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor: theme.segmentedControlTextColor], for: .normal)
    }
    
    private func setTabBarItemColors(_ itemAppearance: UITabBarItemAppearance) {
        itemAppearance.normal.iconColor = theme.toolbarUnselectedAccentColor
        itemAppearance.normal.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.toolbarUnselectedAccentColor]
        
        itemAppearance.selected.iconColor = theme.toolbarAccentColor
        itemAppearance.selected.titleTextAttributes = [NSAttributedString.Key.foregroundColor: theme.toolbarAccentColor]
    }
    #endif
}
#endif
