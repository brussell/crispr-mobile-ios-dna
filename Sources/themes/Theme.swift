//
//  Theme.swift
//  CRISPR
//
//  Created by Branden Russell on 10/28/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

#if canImport(UIKit)
import UIKit

public enum ThemeType: String, CaseIterable {
    case `default`
    case blue
    
    public var displayName: String {
        switch self {
        case .default:
            return NSLocalizedString("Default", comment: "Default theme title")
        case .blue:
            return NSLocalizedString("Blue", comment: "Blue theme title")
        }
    }
}

/// Override this class if you want to create your own named themes instead of those in the base ThemeType
open class Theme {
    /// Default values to set properties to before they're read from the theme file
    public static let notSpecified = UIColor.clear
    
    /// This is the base list of properties used across all apps
    public var accentColor = Theme.notSpecified
    public var backgroundColor = Theme.notSpecified
    public var filledButtonBackgroundColor = Theme.notSpecified
    public var filledButtonBackgroundColorDisabled = Theme.notSpecified
    public var filledButtonBackgroundColorFocused = Theme.notSpecified
    public var filledButtonBackgroundColorSecondary = Theme.notSpecified
    public var filledButtonBackgroundColorSecondaryDisabled = Theme.notSpecified
    public var filledButtonBackgroundColorSecondaryFocused = Theme.notSpecified
    public var filledButtonBackgroundColorTertiary = Theme.notSpecified
    public var filledButtonBackgroundColorTertiaryDisabled = Theme.notSpecified
    public var filledButtonBackgroundColorTertiaryFocused = Theme.notSpecified
    public var filledButtonTextColor = Theme.notSpecified
    public var filledButtonTextColorDisabled = Theme.notSpecified
    public var filledButtonTextColorFocused = Theme.notSpecified
    public var filledButtonTextColorSecondary = Theme.notSpecified
    public var filledButtonTextColorSecondaryDisabled = Theme.notSpecified
    public var filledButtonTextColorSecondaryFocused = Theme.notSpecified
    public var filledButtonTextColorTertiary = Theme.notSpecified
    public var filledButtonTextColorTertiaryDisabled = Theme.notSpecified
    public var filledButtonTextColorTertiaryFocused = Theme.notSpecified
    public var hyperlinkColor = Theme.notSpecified
    public var listBackgroundColor = Theme.notSpecified
    public var listItemBackgroundColor = Theme.notSpecified
    public var listItemSelectedColor = Theme.notSpecified
    public var listItemCollapsedBackgroundColor = Theme.notSpecified
    public var listItemExpandedBackgroundColor = Theme.notSpecified
    public var listSectionHeaderTextColor = Theme.notSpecified
    public var listSeparatorColor = Theme.notSpecified
    public var modalBackgroundColor = Theme.notSpecified
    public var modalBorderColor = Theme.notSpecified
    public var modalShadowColor = Theme.notSpecified
    public var navigationBarAccentColor = Theme.notSpecified
    public var navigationBarBackgroundColor = Theme.notSpecified
    public var navigationBarShadowColor = Theme.notSpecified
    public var navigationBarTitleColor = Theme.notSpecified
    public var navigationBarLargeTitleColor = Theme.notSpecified
    public var segmentedControlSelectedBackgroundColor = Theme.notSpecified
    public var segmentedControlSelectedTextColor = Theme.notSpecified
    public var segmentedControlTextColor = Theme.notSpecified
    public var textColor = Theme.notSpecified
    public var textColorDisabled = Theme.notSpecified
    public var textColorError = Theme.notSpecified
    public var textColorPlaceholder = Theme.notSpecified
    public var textColorSecondary = Theme.notSpecified
    public var textColorSecondaryDisabled = Theme.notSpecified
    public var textColorSuccess = Theme.notSpecified
    public var textColorTertiary = Theme.notSpecified
    public var textColorTertiaryDisabled = Theme.notSpecified
    public var textColorWarning = Theme.notSpecified
    public var toolbarAccentColor = Theme.notSpecified
    public var toolbarBackgroundColor = Theme.notSpecified
    public var toolbarUnselectedAccentColor = Theme.notSpecified

    #if os(iOS)
    public var accessoryViewBarStyle = UIBarStyle.default
    public var separatorStyle = UITableViewCell.SeparatorStyle.none
    public var statusBarStyle = UIStatusBarStyle.default
    #endif
    
    #if !os(watchOS)
    public var keyboardAppearance = UIKeyboardAppearance.default
    #endif
    
    public var customFont: UIFont?
    
    /// Here just to make it so this can be initialized outside the library, if needed
    public init() {
    }
    
    /// Override this method to set colors how you want if you want to create your own theme lists
    /// - Parameter themeName: Name of theme. Provided by ThemeType.displayName
    open func setTheme(with themeName: String) {
        useAssetsToSetColors(with: themeName)
    }
    
    /// Uses the specfied themeName to look through xcasset files to find the colors
    /// - Parameter themeName: The name of theme to get colors for
    public func useAssetsToSetColors(with themeName: String) {
        accentColor = UIColor.color(for: "accent", in: themeName)
        backgroundColor = UIColor.color(for: "background", in: themeName)
        filledButtonBackgroundColor = UIColor.color(for: "filledButtonBackground", in: themeName)
        filledButtonBackgroundColorDisabled = UIColor.color(for: "filledButtonBackgroundDisabled", in: themeName)
        filledButtonBackgroundColorFocused = UIColor.color(for: "filledButtonBackgroundFocused", in: themeName)
        filledButtonBackgroundColorSecondary = UIColor.color(for: "filledButtonBackgroundSecondary", in: themeName)
        filledButtonBackgroundColorSecondaryDisabled = UIColor.color(for: "filledButtonBackgroundSecondaryDisabled", in: themeName)
        filledButtonBackgroundColorSecondaryFocused = UIColor.color(for: "filledButtonBackgroundSecondaryFocused", in: themeName)
        filledButtonBackgroundColorTertiary = UIColor.color(for: "filledButtonBackgroundTertiary", in: themeName)
        filledButtonBackgroundColorTertiaryDisabled = UIColor.color(for: "filledButtonBackgroundTertiaryDisabled", in: themeName)
        filledButtonBackgroundColorTertiaryFocused = UIColor.color(for: "filledButtonBackgroundTertiaryFocused", in: themeName)
        filledButtonTextColor = UIColor.color(for: "filledButtonText", in: themeName)
        filledButtonTextColorDisabled = UIColor.color(for: "filledButtonTextDisabled", in: themeName)
        filledButtonTextColorFocused = UIColor.color(for: "filledButtonTextFocused", in: themeName)
        filledButtonTextColorSecondary = UIColor.color(for: "filledButtonTextSecondary", in: themeName)
        filledButtonTextColorSecondaryDisabled = UIColor.color(for: "filledButtonTextSecondaryDisabled", in: themeName)
        filledButtonTextColorSecondaryFocused = UIColor.color(for: "filledButtonTextSecondaryFocused", in: themeName)
        filledButtonTextColorTertiary = UIColor.color(for: "filledButtonTextTertiary", in: themeName)
        filledButtonTextColorTertiaryDisabled = UIColor.color(for: "filledButtonTextTertiaryDisabled", in: themeName)
        filledButtonTextColorTertiaryFocused = UIColor.color(for: "filledButtonTextTertiaryFocused", in: themeName)
        hyperlinkColor = UIColor.color(for: "hyperlink", in: themeName)
        listBackgroundColor = UIColor.color(for: "listBackground", in: themeName)
        listItemBackgroundColor = UIColor.color(for: "listItemBackground", in: themeName)
        listItemSelectedColor = UIColor.color(for: "listItemSelected", in: themeName)
        listItemCollapsedBackgroundColor = UIColor.color(for: "listItemCollapsedBackground", in: themeName)
        listItemExpandedBackgroundColor = UIColor.color(for: "listItemExpandedBackground", in: themeName)
        listSectionHeaderTextColor = UIColor.color(for: "listSectionHeaderText", in: themeName)
        listSeparatorColor = UIColor.color(for: "listSeparator", in: themeName)
        modalBackgroundColor = UIColor.color(for: "modalBackground", in: themeName)
        modalBorderColor = UIColor.color(for: "modalBorder", in: themeName)
        modalShadowColor = UIColor.color(for: "modalShadow", in: themeName)
        navigationBarAccentColor = UIColor.color(for: "navigationBarAccent", in: themeName)
        navigationBarBackgroundColor = UIColor.color(for: "navigationBarBackground", in: themeName)
        navigationBarShadowColor = UIColor.color(for: "navigationBarShadow", in: themeName)
        navigationBarTitleColor = UIColor.color(for: "navigationBarTitle", in: themeName)
        navigationBarLargeTitleColor = UIColor.color(for: "navigationBarLargeTitle", in: themeName)
        segmentedControlSelectedBackgroundColor = UIColor.color(for: "segmentedControlSelectedBackground", in: themeName)
        segmentedControlSelectedTextColor = UIColor.color(for: "segmentedControlSelectedText", in: themeName)
        segmentedControlTextColor = UIColor.color(for: "segmentedControlText", in: themeName)
        textColor = UIColor.color(for: "text", in: themeName)
        textColorDisabled = UIColor.color(for: "textDisabled", in: themeName)
        textColorError = UIColor.color(for: "textError", in: themeName)
        textColorPlaceholder = UIColor.color(for: "textPlaceholder", in: themeName)
        textColorSecondary = UIColor.color(for: "textSecondary", in: themeName)
        textColorSecondaryDisabled = UIColor.color(for: "textSecondaryDisabled", in: themeName)
        textColorSuccess = UIColor.color(for: "textSuccess", in: themeName)
        textColorTertiary = UIColor.color(for: "textTertiary", in: themeName)
        textColorTertiaryDisabled = UIColor.color(for: "textTertiaryDisabled", in: themeName)
        textColorWarning = UIColor.color(for: "textWarning", in: themeName)
        toolbarAccentColor = UIColor.color(for: "toolbarAccent", in: themeName)
        toolbarBackgroundColor = UIColor.color(for: "toolbarBackground", in: themeName)
        toolbarUnselectedAccentColor = UIColor.color(for: "toolbarUnselectedAccent", in: themeName)
    }
    
    /// Provides the correct font for the theme and style specified
    public func preferredFont(forTextStyle textStyle: UIFont.TextStyle) -> UIFont {
        if let customFont = customFont {
            return UIFontMetrics(forTextStyle: textStyle).scaledFont(for: customFont)
        } else {
            return UIFont.preferredFont(forTextStyle: textStyle)
        }
    }
}

public extension Bundle {
    static var theme: Bundle = {
        return Bundle(url: Bundle.module.bundleURL)!
    }()
}
#endif
