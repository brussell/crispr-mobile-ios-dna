//
//  Color.swift
//  CRISPR
//
//  Created by Branden Russell on 7/29/20.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

#if canImport(UIKit)
import SwiftUI

/// This maps all the colors in the theme to be on Color so they're used more simply and automatically updates views in SwiftUI when the theme changes
public extension Color {
    static var accent: Color { Color(ThemeController.shared.theme.accentColor) }
    static var background: Color { Color(ThemeController.shared.theme.backgroundColor) }
    static var filledButtonBackground: Color { Color(ThemeController.shared.theme.filledButtonBackgroundColor) }
    static var filledButtonBackgroundDisabled: Color { Color(ThemeController.shared.theme.filledButtonBackgroundColorDisabled) }
    static var filledButtonBackgroundFocused: Color { Color(ThemeController.shared.theme.filledButtonBackgroundColorFocused) }
    static var filledButtonBackgroundSecondary: Color { Color(ThemeController.shared.theme.filledButtonBackgroundColorSecondary) }
    static var filledButtonBackgroundSecondaryDisabled: Color { Color(ThemeController.shared.theme.filledButtonBackgroundColorSecondaryDisabled) }
    static var filledButtonBackgroundSecondaryFocused: Color { Color(ThemeController.shared.theme.filledButtonBackgroundColorSecondaryFocused) }
    static var filledButtonBackgroundTertiary: Color { Color(ThemeController.shared.theme.filledButtonBackgroundColorTertiary) }
    static var filledButtonBackgroundTertiaryDisabled: Color { Color(ThemeController.shared.theme.filledButtonBackgroundColorTertiaryDisabled) }
    static var filledButtonBackgroundTertiaryFocused: Color { Color(ThemeController.shared.theme.filledButtonBackgroundColorTertiaryFocused) }
    static var filledButtonText: Color { Color(ThemeController.shared.theme.filledButtonTextColor) }
    static var filledButtonTextDisabled: Color { Color(ThemeController.shared.theme.filledButtonTextColorDisabled) }
    static var filledButtonTextFocused: Color { Color(ThemeController.shared.theme.filledButtonTextColorFocused) }
    static var filledButtonTextSecondary: Color { Color(ThemeController.shared.theme.filledButtonTextColorSecondary) }
    static var filledButtonTextSecondaryDisabled: Color { Color(ThemeController.shared.theme.filledButtonTextColorSecondaryDisabled) }
    static var filledButtonTextSecondaryFocused: Color { Color(ThemeController.shared.theme.filledButtonTextColorSecondaryFocused) }
    static var filledButtonTextTertiary: Color { Color(ThemeController.shared.theme.filledButtonTextColorTertiary) }
    static var filledButtonTextTertiaryDisabled: Color { Color(ThemeController.shared.theme.filledButtonTextColorTertiaryDisabled) }
    static var filledButtonTextTertiaryFocused: Color { Color(ThemeController.shared.theme.filledButtonTextColorTertiaryFocused) }
    static var hyperlink: Color { Color(ThemeController.shared.theme.hyperlinkColor) }
    static var listBackground: Color { Color(ThemeController.shared.theme.listBackgroundColor) }
    static var listItemBackground: Color { Color(ThemeController.shared.theme.listItemBackgroundColor) }
    static var listItemSelected: Color { Color(ThemeController.shared.theme.listItemSelectedColor) }
    static var listItemCollapsedBackground: Color { Color(ThemeController.shared.theme.listItemCollapsedBackgroundColor) }
    static var listItemExpandedBackground: Color { Color(ThemeController.shared.theme.listItemExpandedBackgroundColor) }
    static var listSectionHeaderText: Color { Color(ThemeController.shared.theme.listSectionHeaderTextColor) }
    static var listSeparator: Color { Color(ThemeController.shared.theme.listSeparatorColor) }
    static var modalBackground: Color { Color(ThemeController.shared.theme.modalBackgroundColor) }
    static var modalBorder: Color { Color(ThemeController.shared.theme.modalBorderColor) }
    static var modalShadow: Color { Color(ThemeController.shared.theme.modalShadowColor) }
    static var navigationBarAccent: Color { Color(ThemeController.shared.theme.navigationBarAccentColor) }
    static var navigationBarBackground: Color { Color(ThemeController.shared.theme.navigationBarBackgroundColor) }
    static var navigationBarShadow: Color { Color(ThemeController.shared.theme.navigationBarShadowColor) }
    static var navigationBarTitle: Color { Color(ThemeController.shared.theme.navigationBarTitleColor) }
    static var navigationBarLargeTitle: Color { Color(ThemeController.shared.theme.navigationBarLargeTitleColor) }
    static var segmentedControlSelectedBackground: Color { Color(ThemeController.shared.theme.segmentedControlSelectedBackgroundColor) }
    static var segmentedControlSelectedText: Color { Color(ThemeController.shared.theme.segmentedControlSelectedTextColor) }
    static var segmentedControlText: Color { Color(ThemeController.shared.theme.segmentedControlTextColor) }
    static var text: Color { Color(ThemeController.shared.theme.textColor) }
    static var textDisabled: Color { Color(ThemeController.shared.theme.textColorDisabled) }
    static var textError: Color { Color(ThemeController.shared.theme.textColorError) }
    static var textPlaceholder: Color { Color(ThemeController.shared.theme.textColorPlaceholder) }
    static var textSecondary: Color { Color(ThemeController.shared.theme.textColorSecondary) }
    static var textSecondaryDisabled: Color { Color(ThemeController.shared.theme.textColorSecondaryDisabled) }
    static var textSuccess: Color { Color(ThemeController.shared.theme.textColorSuccess) }
    static var textTertiary: Color { Color(ThemeController.shared.theme.textColorTertiary) }
    static var textTertiaryDisabled: Color { Color(ThemeController.shared.theme.textColorTertiaryDisabled) }
    static var textWarning: Color { Color(ThemeController.shared.theme.textColorWarning) }
    static var toolbarAccent: Color { Color(ThemeController.shared.theme.toolbarAccentColor) }
    static var toolbarBackground: Color { Color(ThemeController.shared.theme.toolbarBackgroundColor) }
    static var toolbarUnselectedAccent: Color { Color(ThemeController.shared.theme.toolbarUnselectedAccentColor) }
}
#endif
