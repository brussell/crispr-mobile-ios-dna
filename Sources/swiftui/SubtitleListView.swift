//
//  ThemeObserverModel.swift
//  CRISPR
//
//  Created by Branden Russell on 11/12/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

#if canImport(UIKit)
import SwiftUI

public struct SubtitleListView: View {
    @EnvironmentObject public var themeController: ThemeController
    
    public var rowInfo: RowInfo
    
    public var body: some View {
        let content = HStack {
            rowInfo.imageInfo?.image
                .resizable()
                .modifyConditionally(rowInfo.imageInfo?.isRounded ?? false) {
                    $0.clipShape(Circle())
                }
                .frame(width: rowInfo.imageInfo?.size?.width, height: rowInfo.imageInfo?.size?.height, alignment: .center)
            
            VStack(alignment: .leading) {
                if let title = rowInfo.title {
                    Text(title)
                        .font(.themeBody)
                        .foregroundColor(.text)
                }
                if let subtitle = rowInfo.subtitle {
                    Text(subtitle)
                        .font(.themeFootnote)
                        .foregroundColor(.textSecondary)
                }
            }
            Spacer()
        }
        
        // This is so tvOS will scroll a List with SubtitleListViews in it
        #if os(tvOS)
            return content.focusable()
        #else
            return content
        #endif
    }
    
    public init(rowInfo: RowInfo) {
        self.rowInfo = rowInfo
    }
}

public struct SubtitleListView_Previews: PreviewProvider {
    public static var previews: some View {
        List {
            SubtitleListView(rowInfo: RowInfo(imageInfo: nil, title: "Test", subtitle: "Subtitle", type: .basic, action: nil))
                .listRowBackground(Color.listBackground)
        }
        .modifier(ThemeModifier())
        .listStyle(PlainListStyle())
    }
}

public enum RowType {
    case basic
}

/// Used to specify a row in a List. Identefiable so that it can be used in a dynamic list automatically
public struct RowInfo: Identifiable {
    public var id = UUID()
    
    let imageInfo: ImageInfo?
    let title: String?
    let subtitle: String?
    let type: RowType
    let action: (() -> Void)?
    
    public init(imageInfo: ImageInfo? = nil, title: String? = nil, subtitle: String? = nil, type: RowType = .basic, action: (() -> Void)? = nil) {
        self.imageInfo = imageInfo
        self.title = title
        self.subtitle = subtitle
        self.type = type
        self.action = action
    }
}

public struct ImageInfo {
    let image: Image
    let isRounded: Bool
    let size: CGSize?
    
    public init(image: Image, isRounded: Bool = false, size: CGSize? = CGSize(width: 44, height: 44)) {
        self.image = image
        self.isRounded = isRounded
        self.size = size
    }
}
#endif
