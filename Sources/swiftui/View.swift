//
//  Color.swift
//  CRISPR
//
//  Created by Branden Russell on 8/14/20.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

#if canImport(UIKit)
import SwiftUI

public extension View {
    /// Conditionally applies modifications to a View
    /// - Parameters:
    ///   - condition: Condition on when the modifications should be applied
    ///   - trueModifier: Modifications to be applied if condition is true
    /// - Returns: View, whether or not it was modified
    func modifyConditionally<Content: View>(_ condition: Bool, trueModifier: ((Self) -> Content)) -> some View {
        Group {
            if condition {
                trueModifier(self)
            } else {
                self
            }
        }
    }
    
    /// Conditionally applies modifications to a View
    /// - Parameters:
    ///   - condition: Condition on when the modifications should be applied
    ///   - trueModifier: Modifications to be applied if condition is true
    ///   - falseModifier: Modifications to be applied if condition is false
    /// - Returns: View after modifications
    func modifyConditionally<TrueContent: View, FalseContent: View>(_ condition: Bool, trueModifier: ((Self) -> TrueContent), falseModifier: ((Self) -> FalseContent)) -> some View {
        Group {
            if condition {
                trueModifier(self)
            } else {
                falseModifier(self)
            }
        }
    }
}
#endif
