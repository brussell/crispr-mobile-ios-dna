//
//  ThemeObserverModel.swift
//  CRISPR
//
//  Created by Branden Russell on 11/11/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

#if canImport(UIKit)
import SwiftUI

/// Apply this modifier to your main view so that it can take care of a lot of the default modifers needed for theming. It will also add the theme into the environment
/// WindowGroup {
///   ContentView()
///      .modifier(ThemeModifier())
///      .onAppear {
///          ThemeController.shared.updateAppearance()
///      }
/// }
public struct ThemeModifier: ViewModifier {
    @EnvironmentObject var themeController: ThemeController
    
    /// Here just to make it so this can be initialized outside the library
    public init() {
    }
    
    public func body(content: Content) -> some View {
        let themedContent = content
            .accentColor(.accent)
            .font(.themeBody)
            .environmentObject(ThemeController.shared)
        
        return Group {
        #if !os(tvOS)
            if #available(iOS 14.0, *) {
                themedContent
                    .toggleStyle(SwitchToggleStyle(tint: .accent))
            } else {
                themedContent
            }
        #else
            themedContent
        #endif
        }
    }
}

public extension Theme {
    /// Returns a theme specified font (or default system font if no custom font is used) for the specified textStyle
    /// - Parameter textStyle: The style of font to use
    /// - Returns: Font based on the specified style and font family
    func font(_ textStyle: UIFont.TextStyle) -> Font {
        let preferredFont = UIFont.preferredFont(forTextStyle: textStyle)
        guard let customFont = customFont else { return Font(preferredFont) }
        
        return .custom(customFont.fontName, size: preferredFont.pointSize)
    }
}

/// This is useful to just be able to use dot syntax at point of use: `.font(.themeBody)`
public extension Font {
    #if !os(tvOS)
    /// System or theme font family at largeTitle textStyle. Default: 34.0 SFUIDisplay
    static var themeLargeTitle: Font { ThemeController.shared.theme.font(.largeTitle) }
    /// System or theme font family at title1 textStyle. Default: 28.0 SFUIDisplay
    #endif
    
    static var themeTitle1: Font { ThemeController.shared.theme.font(.title1) }
    /// System or theme font family at title2 textStyle. Default: 22.0 SFUIDisplay
    static var themeTitle2: Font { ThemeController.shared.theme.font(.title2) }
    /// System or theme font family at title3 textStyle. Default: 20.0 SFUIDisplay
    static var themeTitle3: Font { ThemeController.shared.theme.font(.title3) }
    /// System or theme font family at headline textStyle. Default: 17.0 SFUIText-Semibold
    static var themeHeadline: Font { ThemeController.shared.theme.font(.headline) }
    /// System or theme font family at subheadline textStyle. Default: 15.0 SFUIText
    static var themeSubheadline: Font { ThemeController.shared.theme.font(.subheadline) }
    /// System or theme font family at body textStyle. Default: 17.0 SFUIText
    static var themeBody: Font { ThemeController.shared.theme.font(.body) }
    /// System or theme font family at callout textStyle. Default: 16.0 SFUIText
    static var themeCallout: Font { ThemeController.shared.theme.font(.callout) }
    /// System or theme font family at footnote textStyle. Default: 13.0 SFUIText
    static var themeFootnote: Font { ThemeController.shared.theme.font(.footnote) }
    /// System or theme font family at caption1 textStyle. Default: 12.0 SFUIText
    static var themeCaption1: Font { ThemeController.shared.theme.font(.caption1) }
    /// System or theme font family at caption2 textStyle. Default: 11.0 SFUIText
    static var themeCaption2: Font { ThemeController.shared.theme.font(.caption2) }
}
#endif
