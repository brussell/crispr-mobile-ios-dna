//
//  Logger.swift
//  CRISPR
//
//  Created by Branden Russell on 10/28/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

import Foundation

public enum LogLevel {
    case fatal
    case error
    case warn
    case debug
    case info
}

public struct Logger {
    
    private static func sourceFileName(from filePath: String) -> String {
        let components = filePath.components(separatedBy: "/")
        return components.isEmpty ? "" : components.last ?? ""
    }
    
    private static func currentTimeStamp() -> String {
        let unixTimestamp = Date().timeIntervalSince1970
        let date = Date(timeIntervalSince1970: unixTimestamp)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "HH:mm:ss.SS"
        return dateFormatter.string(from: date)
    }
    
    /// Log the specified message with specefied level
    public static func logWith(logLevel: LogLevel, message: Any?, file: String =  #file, line: Int = #line, column: Int = #column, function: String = #function) {
        let fileName = sourceFileName(from: file)
        let location = "File: \(fileName), Line: \(line), Col: \(column), Func: \(function)"
        print("\(logLevel) - \(location) - \(currentTimeStamp()) - \(String(describing: message))")
    }
    
}
