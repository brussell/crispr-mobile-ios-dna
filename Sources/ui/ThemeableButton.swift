//
//  ThemeableButton.swift
//  CRISPR
//
//  Created by Branden Russell on 10/30/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

#if canImport(UIKit) && !os(watchOS)
import UIKit
import Combine

/// Button with touch handlers. Handles themeing in multple styles
open class ThemeableButton: UIButton {
    public enum Style {
        case filled
        case standard
    }
    
    private var cancellables = Set<AnyCancellable>()
    
    var touchDownHandler: ((_ button: ThemeableButton) -> Void)?
    var touchUpHandler: ((_ button: ThemeableButton) -> Void)?
    var style = Style.standard
    
    public init(style: Style = .standard, touchDownHandler: ((_ button: ThemeableButton) -> Void)? = nil, touchUpHandler: ((_ button: ThemeableButton) -> Void)? = nil) {
        super.init(frame: .zero)
        configure(style: style, touchDownHandler: touchDownHandler, touchUpHandler: touchUpHandler)
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        configure()
    }
    
    @objc func touchDown() {
        #if os(iOS)
            UIImpactFeedbackGenerator(style: .light).impactOccurred(intensity: 0.5)
        #endif
        
        touchDownHandler?(self)
    }
    
    @objc func touchUp() {
        touchUpHandler?(self)
    }
    
    private func configure(style: Style = .standard, touchDownHandler: ((_ button: ThemeableButton) -> Void)? = nil, touchUpHandler: ((_ button: ThemeableButton) -> Void)? = nil) {
        self.style = style
        self.touchDownHandler = touchDownHandler
        self.touchUpHandler = touchUpHandler
        addTarget(self, action: #selector(touchDown), for: .touchDown)
        addTarget(self, action: #selector(touchUp), for: .touchUpInside)
        addTarget(self, action: #selector(touchUp), for: .touchUpOutside)
        addTarget(self, action: #selector(touchUp), for: .touchCancel)
        
        translatesAutoresizingMaskIntoConstraints = false
        
        ThemeController.shared.$themeName
            .map { _ in ThemeController.shared.theme }
            .sink(receiveValue: { [weak self] theme in
                self?.titleLabel?.font = theme.preferredFont(forTextStyle: .body)
    
                switch style {
                case .filled:
                    self?.backgroundColor = theme.filledButtonBackgroundColor
                    self?.setTitleColor(theme.filledButtonTextColor, for: .normal)
                    self?.tintColor = theme.filledButtonTextColor
                case .standard:
                    self?.backgroundColor = theme.backgroundColor
                    self?.setTitleColor(theme.textColor, for: .normal)
                    self?.tintColor = theme.textColor
                }
            })
            .store(in: &cancellables)
    }    
}
#endif
