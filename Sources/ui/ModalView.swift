//
//  ModalView.swift
//  CRISPR
//
//  Created by Branden Russell on 10/30/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

#if canImport(UIKit) && !os(watchOS)
import UIKit
import Combine

/// A custom modal alert view. Supports an array of buttons. Supports an array of message content as strings and/or images. Mupltile presentation styles
open class ModalView: UIView {
    private var cancellables = Set<AnyCancellable>()
    
    /// Default cancel button
    public static let cancelButtonInfo: ButtonInfo = {
        return ButtonInfo(title: NSLocalizedString("Cancel", comment: "Cancel button"), action: nil)
    }()
    
    /// Default Ok button
    public static let okButtonInfo: ButtonInfo = {
        return ButtonInfo(title: NSLocalizedString("Ok", comment: "Okay button"), action: nil)
    }()
    
    /// Default continue button
    public static let continueButtonInfo: ButtonInfo = {
        return ButtonInfo(title: NSLocalizedString("Continue", comment: "Continue button"), action: nil)
    }()
    
    /// Defines what a button says and what it does when tapped
    public struct ButtonInfo {
        public let title: String
        public let action: (() -> Void)?
        
        public init (title: String, action: (() -> Void)? = nil) {
            self.title = title
            self.action = action
        }
    }
    
    /// Various presentation styles
    public enum ModalType {
        /// Spins on to the screen from a random outside the screen (if not given a point of origin) point and orientation to the center of the screen. Spins off at a different random orientation and point off screen when dismissed (if not given a point of origin)
        case fun
        /// Animates growing from the center of a point of orgin (or default center of the screen) to the center of the screen. Reverses animation when dismissed
        case standard
    }
    
    /// Useful for dimming the rest of the screen
    private lazy var backgroundView: UIView = {
        let view = UIView()
        view.accessibilityIdentifier = "ModalView-BackgroundView"
        return view
    }()
    /// Holds all the views of the modal
    private lazy var modalView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.accessibilityIdentifier = "ModalView-ModalView"
        view.layer.cornerRadius = 14
        view.layer.shadowOffset = CGSize(width: 0, height: 6)
        view.layer.shadowOpacity = 0.5
        view.layer.shadowRadius = 3
        view.layer.shadowColor = UIColor.black.cgColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    /// Optional label across the top of the modal
    private lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.adjustsFontForContentSizeCategory = true
        return titleLabel
    }()
    /// Optional image across the top of the modal that projects out of the modal a bit
    private var titleImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalToConstant: 166),
            imageView.widthAnchor.constraint(equalToConstant: 166),
        ])
        return imageView
    }()
    /// Called no matter how the modal is dismissed
    private var dismissedCompletion: (() -> Void)?
    /// An array of buttonInfos. If there are two or fewer, they're presented side by side. Three or more are presented vertically
    private var buttonInfos = [ButtonInfo]()
    /// Buttons created from buttonInfos
    private var buttons = [ThemeableButton]()
    /// Constraint to track the origin point
    private var hidingCenterXConstraint: NSLayoutConstraint?
    /// Constraint to track the origin point
    private var hidingCenterYConstraint: NSLayoutConstraint?
    /// Constraint to track the destination point
    private var showingLeadingConstraint: NSLayoutConstraint?
    /// Constraint to track the destination point
    private var showingTrailingConstraint: NSLayoutConstraint?
    /// Constraint to track the destination point
    private var showingCenterYConstraint: NSLayoutConstraint?
    /// Constraint to track the image for in the case of fun presentation, the image animates in separatly
    private var titleImageViewYConstraint: NSLayoutConstraint?
    /// Presentation style
    private var style = ModalType.standard
    /// An array of the message contents for the labels and image viwes
    private var formattedContents = [UIView]()
    
    public convenience init(style: ModalType = .standard, title: String? = nil, titleImage: UIImage? = nil, body: [Any]? = nil, buttonInfos: [ButtonInfo] = [ModalView.continueButtonInfo], dismissable: Bool = true, dismissedCompletion: (() -> Void)? = nil) {
        self.init(frame: UIScreen.main.bounds)
        self.style = style
        self.buttonInfos = buttonInfos
        self.dismissedCompletion = dismissedCompletion
        createModal(with: style, title: title, titleImage: titleImage, body: body, buttonInfos: buttonInfos, dismissable: dismissable)
        accessibilityIdentifier = "ModalView-View"
        
        ThemeController.shared.$themeName
            .map { _ in ThemeController.shared.theme }
            .sink(receiveValue: { [weak self] theme in
                self?.titleLabel.textColor = theme.textColor
                self?.modalView.backgroundColor = theme.modalBackgroundColor
                self?.modalView.borderColor = theme.modalBorderColor
                self?.layer.shadowColor = theme.modalShadowColor.cgColor

                self?.formattedContents.forEach { view in
                    guard let label = view as? UILabel else { return }
                    label.textColor = theme.textColor
                    label.font = theme.preferredFont(forTextStyle: .body)
                }
                self?.buttons.forEach { button in
                    button.titleLabel?.font = theme.preferredFont(forTextStyle: .body)
                }
                self?.titleLabel.font = theme.preferredFont(forTextStyle: .headline)
            })
            .store(in: &cancellables)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    /// Creates a modal with specified parameters
    private func createModal(with style: ModalType, title: String?, titleImage: UIImage? = nil, body: [Any]? = nil, buttonInfos: [ButtonInfo], dismissable: Bool) {
        backgroundView.frame = frame
        
        if dismissable {
            backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedOnBackgroundView)))
            modalView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedOnBackgroundView)))
        }
        addSubview(backgroundView)
        
        titleLabel.text = title
        modalView.addSubview(titleLabel)
        
        formattedContents = (body ?? []).compactMap { item in
            if let item = item as? String {
                let label = UILabel()
                label.numberOfLines = 0
                label.textAlignment = .center
                label.lineBreakMode = .byWordWrapping
                label.font = .preferredFont(forTextStyle: .body)
                label.text = item
                label.adjustsFontForContentSizeCategory = true
                label.translatesAutoresizingMaskIntoConstraints = false
                return label
            } else if let image = item as? UIImage {
                let imageView = UIImageView(image: image)
                imageView.contentMode = .scaleAspectFit
                imageView.clipsToBounds = true
                imageView.translatesAutoresizingMaskIntoConstraints = false
                return imageView
            } else {
                return nil
            }
        }
        
        let contentStackView = UIStackView(arrangedSubviews: formattedContents)
        contentStackView.translatesAutoresizingMaskIntoConstraints = false
        contentStackView.axis = .vertical
        contentStackView.alignment = .center
        contentStackView.distribution = .equalSpacing
        contentStackView.spacing = 14
        modalView.addSubview(contentStackView)
        
        var buttonStackView = UIStackView()
        buttons = buttonInfos.map { buttonInfo in
            let button = ThemeableButton()
            button.setTitle(buttonInfo.title, for: .normal)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.titleLabel?.font = .preferredFont(forTextStyle: .body)
            button.titleLabel?.adjustsFontForContentSizeCategory = true
            button.accessibilityIdentifier = "Modal \(buttonInfo.title)"
            button.touchUpHandler = { [weak self] button in
                self?.buttonTapped(sender: button)
            }
            
            return button
        }
        buttonStackView = UIStackView(arrangedSubviews: buttons)
        buttonStackView.axis = buttons.count < 3 ? .horizontal : .vertical
        buttonStackView.spacing = buttons.count < 3 ? 4 : 14
        buttonStackView.alignment = buttons.count < 3 ? .center : .fill
        buttonStackView.distribution = .fillEqually
        buttonStackView.translatesAutoresizingMaskIntoConstraints = false
        modalView.addSubview(buttonStackView)

        switch style {
        case .standard:
            modalView.borderWidth = 0
        case .fun:
            modalView.borderWidth = 0
        }
        setupBackgroundDarkening()
        addSubview(modalView)
        
        titleImageView.image = titleImage
        addSubview(titleImageView)
        
        let titleImageViewYConstraint = titleImageView.centerYAnchor.constraint(equalTo: modalView.topAnchor, constant: -1000)
        self.titleImageViewYConstraint = titleImageViewYConstraint
        let titleConstant = titleImage == nil ? 20 : (166 / 2) - 20
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: modalView.topAnchor, constant: CGFloat(titleConstant)),
            titleLabel.leadingAnchor.constraint(equalTo: modalView.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: modalView.trailingAnchor),
            titleLabel.bottomAnchor.constraint(equalTo: contentStackView.topAnchor, constant: -20),
            titleImageView.centerXAnchor.constraint(equalTo: titleLabel.centerXAnchor),
            titleImageViewYConstraint,
            contentStackView.leadingAnchor.constraint(equalTo: modalView.leadingAnchor, constant: 16),
            contentStackView.trailingAnchor.constraint(equalTo: modalView.trailingAnchor, constant: -16),
            contentStackView.bottomAnchor.constraint(equalTo: buttonStackView.topAnchor, constant: -16),
            buttonStackView.leadingAnchor.constraint(equalTo: modalView.leadingAnchor, constant: 16),
            buttonStackView.trailingAnchor.constraint(equalTo: modalView.trailingAnchor, constant: -16),
            buttonStackView.bottomAnchor.constraint(equalTo: modalView.bottomAnchor, constant: -10)
        ])
        showingLeadingConstraint = modalView.leadingAnchor.constraint(equalTo: readableContentGuide.leadingAnchor, constant: 16)
        showingTrailingConstraint = modalView.trailingAnchor.constraint(equalTo: readableContentGuide.trailingAnchor, constant: -16)
        showingCenterYConstraint = modalView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -50)
        backgroundView.alpha = 0
    }
    
    /// Called if the modal allows tapping the background to dismiss
    @objc func tappedOnBackgroundView() {
        dismiss(animated: true)
    }
    
    /// Called when any butten is tapped
    @objc func buttonTapped(sender: ThemeableButton) {
        guard let index = buttons.firstIndex(of: sender) else { return }
        
        buttonInfos[index].action?()
        dismiss(animated: true)
    }
    
    /// Presents the modal, optionally animated
    public func show(animated: Bool, in window: UIWindow, fromCenterOf fromView: UIView? = nil) {
        DispatchQueue.main.async {
            if var topViewController = window.rootViewController {
                repeat {
                    if let presentedViewController = topViewController.presentedViewController {
                        topViewController = presentedViewController
                    } else {
                        topViewController.view.addSubview(self)
                    }
                } while self.superview == nil
            }
            
            self.showingLeadingConstraint?.isActive = true
            self.showingTrailingConstraint?.isActive = true
            self.showingCenterYConstraint?.isActive = true
            self.layoutIfNeeded()
            
            switch self.style {
            case .standard:
                self.hidingCenterXConstraint = self.modalView.centerXAnchor.constraint(equalTo: (fromView ?? self).centerXAnchor)
                self.hidingCenterYConstraint = self.modalView.centerYAnchor.constraint(equalTo: (fromView ?? self).centerYAnchor)
            case .fun:
                switch Int.random(in: 0..<4) {
                case 0: // Top
                    self.hidingCenterXConstraint = self.modalView.centerXAnchor.constraint(equalTo: self.leadingAnchor, constant: CGFloat.random(in: 0..<self.frame.width))
                    self.hidingCenterYConstraint = self.modalView.bottomAnchor.constraint(equalTo: self.topAnchor)
                case 1: // Right
                    self.hidingCenterXConstraint = self.modalView.leadingAnchor.constraint(equalTo: self.trailingAnchor)
                    self.hidingCenterYConstraint = self.modalView.topAnchor.constraint(equalTo: self.topAnchor, constant: CGFloat.random(in: 0..<self.frame.height))
                case 2: // Bottom
                    self.hidingCenterXConstraint = self.modalView.centerXAnchor.constraint(equalTo: self.leadingAnchor, constant: CGFloat.random(in: 0..<self.frame.width))
                    self.hidingCenterYConstraint = self.modalView.topAnchor.constraint(equalTo: self.bottomAnchor)
                case _: // Left
                    self.hidingCenterXConstraint = self.modalView.trailingAnchor.constraint(equalTo: self.leadingAnchor)
                    self.hidingCenterYConstraint = self.modalView.topAnchor.constraint(equalTo: self.topAnchor, constant: CGFloat.random(in: 0..<self.frame.height))
                }
            }
            self.showingLeadingConstraint?.isActive = false
            self.showingTrailingConstraint?.isActive = false
            self.showingCenterYConstraint?.isActive = false
            self.hidingCenterXConstraint?.isActive = true
            self.hidingCenterYConstraint?.isActive = true
            switch self.style {
            case .standard:
                self.modalView.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
            case .fun:
                self.modalView.transform = CGAffineTransform(rotationAngle: CGFloat.random(in: -180..<180))
            }
            self.layoutIfNeeded()
            
            self.hidingCenterXConstraint?.isActive = false
            self.hidingCenterYConstraint?.isActive = false
            self.showingLeadingConstraint?.isActive = true
            self.showingTrailingConstraint?.isActive = true
            self.showingCenterYConstraint?.isActive = true
            
            UIView.animate(withDuration: animated ? 0.2 : 0) {
                self.backgroundView.alpha = 0.66
            }
            UIView.animate(withDuration: animated ? 0.5 : 0, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: [], animations: {
                switch self.style {
                case .standard:
                    self.modalView.transform = CGAffineTransform(scaleX: 1, y: 1)
                case .fun:
                    self.modalView.transform = CGAffineTransform(rotationAngle: 0)
                }
                self.titleImageViewYConstraint?.constant = 0
                self.layoutIfNeeded()
            })
        }
    }
    
    /// Call to dismiss the modal, optionally animated
    public func dismiss(animated: Bool) {
        DispatchQueue.main.async {
            if self.style == .fun {
                switch Int.random(in: 0..<4) {
                case 0: // Top
                    self.hidingCenterXConstraint = self.modalView.centerXAnchor.constraint(equalTo: self.leadingAnchor, constant: CGFloat.random(in: 0..<self.frame.width))
                    self.hidingCenterYConstraint = self.modalView.bottomAnchor.constraint(equalTo: self.topAnchor)
                case 1: // Right
                    self.hidingCenterXConstraint = self.modalView.leadingAnchor.constraint(equalTo: self.trailingAnchor)
                    self.hidingCenterYConstraint = self.modalView.topAnchor.constraint(equalTo: self.topAnchor, constant: CGFloat.random(in: 0..<self.frame.height))
                case 2: // Bottom
                    self.hidingCenterXConstraint = self.modalView.centerXAnchor.constraint(equalTo: self.leadingAnchor, constant: CGFloat.random(in: 0..<self.frame.width))
                    self.hidingCenterYConstraint = self.modalView.topAnchor.constraint(equalTo: self.bottomAnchor)
                case _: // Left
                    self.hidingCenterXConstraint = self.modalView.trailingAnchor.constraint(equalTo: self.leadingAnchor)
                    self.hidingCenterYConstraint = self.modalView.topAnchor.constraint(equalTo: self.topAnchor, constant: CGFloat.random(in: 0..<self.frame.height))
                }
            }
            self.showingLeadingConstraint?.isActive = false
            self.showingTrailingConstraint?.isActive = false
            self.showingCenterYConstraint?.isActive = false
            self.hidingCenterXConstraint?.isActive = true
            self.hidingCenterYConstraint?.isActive = true
            
            let duration = animated ? 0.3 : 0
            UIView.animate(withDuration: duration, animations: {
                self.backgroundView.alpha = 0
                switch self.style {
                case .standard:
                    self.modalView.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
                case .fun:
                    self.modalView.transform = CGAffineTransform(rotationAngle: CGFloat.random(in: -180..<180))
                }
                self.layoutIfNeeded()
            }, completion: { completed in
                self.dismissedCompletion?()
                self.removeFromSuperview()
            })
        }
    }
    
    /// Sets up the background shadow if the modal should have one
    private func setupBackgroundShadow() {
        layer.shadowRadius = 10
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize(width: 2, height: 7)
    }
    
    /// Sets up the darkened background if the modal should have one
    private func setupBackgroundDarkening() {
        backgroundView.backgroundColor = .black
        backgroundView.alpha = 0.5
    }
}
#endif
