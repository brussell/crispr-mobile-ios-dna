//
//  UIViewController.swift
//  CRISPR
//
//  Created by Branden Russell on 10/30/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

#if canImport(UIKit) && !os(watchOS)
import UIKit

extension UIViewController: UITextFieldDelegate {
    /// Sets the next text field as first responder. If there is no next text field, it resigns first responder
    open func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.nextField?.becomeFirstResponder() != true {
            textField.resignFirstResponder()
        }
        
        return true
    }
}

public extension UIViewController {
    /// Shows the modal at the top view controller in the current window
    func show(_ modal: ModalView, animated: Bool) {
        guard let window = UIApplication.shared.delegate?.window ?? view.window else { return }
        
        modal.show(animated: true, in: window)
    }
}
#endif
