//
//  BarButtonItemWithTapHandler.swift
//  CRISPR
//
//  Created by Branden Russell on 10/30/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

#if canImport(UIKit) && !os(watchOS)
import UIKit

/// Adds a closure for when the bar button item is tapped
open class BarButtonItemWithTapHandler: UIBarButtonItem {
    
    private var tapHandler: ((UIBarButtonItem) -> Void)?
    
    /// Used to configure the closure called when the button is tapped
    public func configure(tapHandler: @escaping (UIBarButtonItem) -> Void) {
        self.tapHandler = tapHandler
        target = self
        action = #selector(tapped)
    }
    
    /// Called when the button is tapped. Calls the tapHandler
    @objc public func tapped() {
        tapHandler?(self)
    }
    
}
#endif
