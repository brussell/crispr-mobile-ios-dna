//
//  UITextField.swift
//  CRISPR
//
//  Created by Branden Russell on 10/30/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

#if canImport(UIKit) && !os(watchOS)
import UIKit

public extension UITextField {
    
    /// The color of the placeholder
    @IBInspectable var placeholderColor: UIColor? {
        get {
            return attributedPlaceholder?.attribute(.foregroundColor, at: 0, effectiveRange: nil) as? UIColor
        }
        set {
            guard let attributedPlaceholder = attributedPlaceholder else { return }
            
            if let newValue = newValue {
                self.attributedPlaceholder = NSAttributedString(string: attributedPlaceholder.string, attributes: [.foregroundColor: newValue])
            } else {
                self.attributedPlaceholder = NSAttributedString(string: attributedPlaceholder.string, attributes: nil)
            }
        }
    }
    
}

/// Add a toolbar to the textfield as an accessory input view
public extension UITextField {
    #if !os(tvOS)
    /// Adds an accessory view with a Done button
    /// - Parameter tapHandler: The closure that is called when Done is tapped
    func addDoneButtonAccessoryView(tapHandler: @escaping (UIBarButtonItem) -> Void) {
        addAccessoryView(with: [(buttonTitle: NSLocalizedString("Done", comment: "Done button"), tapHandler: tapHandler)])
    }
    
    /// Adds an accessory view with a Next button
    /// - Parameter tapHandler: The closure that is called when Next is tapped
    func addNextButtonAccessoryView(tapHandler: @escaping (UIBarButtonItem) -> Void) {
        addAccessoryView(with: [(buttonTitle: NSLocalizedString("Next", comment: "Button to go to the next step"), tapHandler: tapHandler)])
    }
    
    /// Adds an accessory view to the keyboard
    /// - Parameter buttonTitlesAndActions: The buttons and closures to add to the accessory view
    func addAccessoryView(with buttonTitlesAndActions: [(buttonTitle: String, tapHandler: (UIBarButtonItem) -> Void)]) {
        let toolbar = UIToolbar()
        toolbar.barStyle = ThemeController.shared.theme.accessoryViewBarStyle
        toolbar.barTintColor = ThemeController.shared.theme.backgroundColor
        toolbar.tintColor = ThemeController.shared.theme.accentColor
        toolbar.isTranslucent = false
        toolbar.sizeToFit()
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let buttons = [flexSpace] + buttonTitlesAndActions.map { buttonTitle, tapHandler in
            let button = BarButtonItemWithTapHandler(title: buttonTitle, style: .plain, target: nil, action: nil)
            button.configure(tapHandler: tapHandler)
            return button
        }
        toolbar.setItems(buttons, animated: true)
        inputAccessoryView = toolbar
    }
    #endif
}

private var kAssociationKeyNextField: UInt8 = 0
public extension UITextField {
    /// Used to track which field should become the next responder when the user returns from this one
    var nextField: UITextField? {
        get {
            return objc_getAssociatedObject(self, &kAssociationKeyNextField) as? UITextField
        }
        
        set {
            objc_setAssociatedObject(self, &kAssociationKeyNextField, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
}
#endif
