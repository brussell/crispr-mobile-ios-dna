//
//  TextViewWithPlaceholder.swift
//  CRISPR
//
//  Created by Branden Russell on 12/17/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

#if canImport(UIKit) && !os(watchOS)
import UIKit
import Combine

/// Designable  text view that adds ability to have a placeholder and handles theme support
@IBDesignable open class TextViewWithPlaceholder: UITextView {
    /// Placeholder text
    @IBInspectable open var placeholderText: String?
    
    /// Label to display the placeholder
    private let placeholderLabel = UILabel()
    
    private var cancellables = Set<AnyCancellable>()
    
    override open var font: UIFont? {
        didSet {
            updatePlaceholder()
        }
    }
    
    override open var contentInset: UIEdgeInsets {
        didSet {
            updatePlaceholder()
        }
    }
    
    override open var textAlignment: NSTextAlignment {
        didSet {
            updatePlaceholder()
        }
    }
    
    override open var text: String? {
        didSet {
            updatePlaceholder()
        }
    }
    
    override open var attributedText: NSAttributedString? {
        didSet {
            updatePlaceholder()
        }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override public init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
        setup()
    }

    convenience public init(placeholderText: String) {
        self.init(frame: .zero)
        self.placeholderText = placeholderText
        self.accessibilityLabel = placeholderText
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
        setup()
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        updatePlaceholder()
    }
    
    // MARK: - Methods
    
    private func setup() {
        translatesAutoresizingMaskIntoConstraints = false
        
        NotificationCenter.default.removeObserver(self, name: UITextView.textDidChangeNotification, object: self)
        NotificationCenter.default.addObserver(self, selector: #selector(TextViewWithPlaceholder.textChanged(_:)), name: UITextView.textDidChangeNotification, object: self)
        
        addSubview(placeholderLabel)
        
        ThemeController.shared.$themeName
            .map { _ in ThemeController.shared.theme }
            .sink(receiveValue: { [weak self] theme in
                self?.font = theme.preferredFont(forTextStyle: .body)
                self?.textColor = theme.textColor
                self?.tintColor = theme.accentColor
                self?.placeholderLabel.textColor = theme.textColorPlaceholder
                self?.placeholderLabel.font = self?.font
    
                self?.updatePlaceholder()
            })
            .store(in: &cancellables)
    }
    
    /// Updates the placement and visibility of the placeholder label
    private func updatePlaceholder() {
        placeholderLabel.text = placeholderText
        placeholderLabel.frame = placeholderRect()
        placeholderLabel.isHidden = !text.isNilOrEmpty || placeholderText == nil
    }
    
    /// Attributed string values for the placeholder text
    private func placeholderTextAttributes() -> [NSAttributedString.Key: Any] {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = textAlignment
        return [
            NSAttributedString.Key.font: font ?? UIFont.preferredFont(forTextStyle: .body),
            NSAttributedString.Key.foregroundColor: placeholderLabel.textColor ?? .clear,
            NSAttributedString.Key.paragraphStyle: paragraphStyle
        ]
    }
    
    /// Calculates the frame for the placeholder label
    private func placeholderRect() -> CGRect {
        guard let placeholderText = placeholderText else { return .zero }
        
        let documentStartRect = super.caretRect(for: beginningOfDocument)
        let textSize = placeholderText.size(withAttributes: placeholderTextAttributes())
        switch textAlignment {
        case .center:
            return CGRect(x: documentStartRect.origin.x - (textSize.width / 2.0), y: documentStartRect.origin.y, width: textSize.width, height: textSize.height)
        case .right:
            return CGRect(x: documentStartRect.origin.x - textSize.width, y: documentStartRect.origin.y, width: textSize.width, height: textSize.height)
        case .justified, .left, .natural:
            fallthrough
        @unknown default:
            return CGRect(x: documentStartRect.origin.x, y: documentStartRect.origin.y, width: textSize.width, height: textSize.height)
        }
    }
    
    // MARK: - Observers
    
    @objc func textChanged(_ notification: Notification) {
        updatePlaceholder()
    }
    
    // MARK: - UITextInput Protocol Methods
    
    override open func caretRect(for position: UITextPosition?) -> CGRect {
        guard let position = position else { return .zero }
        
        let originalRect = super.caretRect(for: position)
        let rect: CGRect
        if text.isNilOrEmpty && placeholderText != nil {
            let placeholderRect = self.placeholderRect()
            rect = CGRect(x: placeholderRect.origin.x, y: placeholderRect.origin.y, width: originalRect.size.width, height: originalRect.size.height)
        } else {
            rect = originalRect
        }
        
        return rect
    }    
}
#endif
