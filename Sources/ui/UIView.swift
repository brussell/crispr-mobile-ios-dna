//
//  UIView.swift
//  CRISPR
//
//  Created by Branden Russell on 10/30/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

#if canImport(UIKit) && !os(watchOS)
import UIKit

/// Extension of view to add some handy CGLayer properties
public extension UIView {
    /// Corner radius of the view. Gets/Sets the value on the CGLayer
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    /// Border width of the view. Gets/Sets the value on the CGLayer
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    /// Border color of the view. Gets/Sets the value on the CGLayer
    @IBInspectable var borderColor: UIColor? {
        get {
            return layer.borderColor.flatMap { UIColor(cgColor: $0) }
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}

public extension UIView {
    /// Rotates the view so the top of the view points towards a specified point. Useful for when a view needs to point towards another
    func rotateVersus(destPoint: CGPoint) {
        let halfWidth = frame.width / 2
        let halfHeight = frame.height / 2
        let vector1 = CGVector(dx: 0, dy: 1)
        let vector2 = CGVector(dx: destPoint.x - frame.origin.x - halfWidth, dy: destPoint.y - frame.origin.y - halfHeight)
        let angle = atan2(vector2.dy, vector2.dx) - atan2(vector1.dy, vector1.dx)
        transform = CGAffineTransform(rotationAngle: angle)
    }
}
#endif
