//
//  ThemeableNavigationController.swift
//  CRISPR
//
//  Created by Branden Russell on 11/7/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

#if canImport(UIKit) && !os(watchOS)
import UIKit
import Combine

/// Handles themeing the navigation controller. Allows hiding title of back button
open class ThemeableNavigationController: UINavigationController {
    private var cancellables = Set<AnyCancellable>()
    
    public var hideBackButtonTitle = true
    
    /// Custom back button without a title
    open lazy var backBarButtonItem: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        return barButtonItem
    }()
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    public override init(navigationBarClass: AnyClass?, toolbarClass: AnyClass?) {
        super.init(navigationBarClass: navigationBarClass, toolbarClass: toolbarClass)
        setup()
    }
    
    public override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        setup()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    #if !os(tvOS)
    /// Uses the top view controller's preference as an overide to the current theme
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? ThemeController.shared.theme.statusBarStyle
    }
    
    /// Uses the top view controller's preference as an overide to all
    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return topViewController?.supportedInterfaceOrientations ?? .all
    }
    
    /// Overrides standard to put custom back button in, if desired
    open override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if hideBackButtonTitle {
            viewController.navigationItem.backBarButtonItem = backBarButtonItem
        }
        super.pushViewController(viewController, animated: animated)
    }
    
    /// Overrides standard to put custom back button in, if desired
    open override func setViewControllers(_ viewControllers: [UIViewController], animated: Bool) {
        super.setViewControllers(viewControllers, animated: animated)
        
        if hideBackButtonTitle {
            navigationItem.backBarButtonItem = backBarButtonItem
            viewControllers.forEach { viewController in
                viewController.navigationItem.backBarButtonItem = backBarButtonItem
            }
        }
    }
    #endif
    
    /// Sets up themeing
    open func setup() {
        ThemeController.shared.$themeName
            .map { _ in ThemeController.shared.theme }
            .sink(receiveValue: { [weak self] theme in
                // Appeareance only affects things that come into the hierarchy. Toggle visibility of navigation bar, if needed, to get it to take on the updated appearance
                if self?.isNavigationBarHidden == false {
                    self?.isNavigationBarHidden = true
                    self?.isNavigationBarHidden = false
                }
            })
            .store(in: &cancellables)
    }    
}
#endif
