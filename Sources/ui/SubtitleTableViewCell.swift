//
//  SubtitleTableViewCell.swift
//  CRISPR
//
//  Created by Branden Russell on 10/30/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

#if canImport(UIKit) && !os(watchOS)
import UIKit
import Combine

/// Generic table view cell that supports an image, title, and subtitle. Handles themeing. Provides automatic row sizing and accessibilty support
public class SubtitleTableViewCell: UITableViewCell {
    private var cancellables = Set<AnyCancellable>()
    private var leadingImageHeightConstraint = NSLayoutConstraint()
    private var leadingImageWidthConstraint = NSLayoutConstraint()
    /// Image view displayed on the leading side of the cell
    public var leadingImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    /// Sets a specific size to make the image view
    public var leadingImageSize = CGSize(width: 0, height: 42) {
        didSet {
            setNeedsUpdateConstraints()
        }
    }
    
    /// Whether to round the leading image view
    public var isLeadingImageRounded: Bool = false {
        didSet {
            setNeedsLayout()
        }
    }
    
    /// How much to indent the leading edge of the text
    public var textIndent: CGFloat = 0 {
        didSet {
            setNeedsUpdateConstraints()
        }
    }
    
    private var descriptionLabelLeadingConstraint = NSLayoutConstraint()
    /// The main label in the cell
    public var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontForContentSizeCategory = true
        
        return label
    }()
    
    /// Replaces access to the default textLabel with this cell's descriptionLabel
    public override var textLabel: UILabel? {
        get {
            return descriptionLabel
        }
        set {
            if let textLabel = newValue {
                descriptionLabel = textLabel
                configureView()
            }
        }
    }
    
    private var subtitleLabelLeadingConstraint = NSLayoutConstraint()
    /// The subtitle label. Handles adjusting priorities to best support resizing the cell for accessibility font sizes. Works with changing numberOfLines here or at usage
    public var subtitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontForContentSizeCategory = true
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        label.setContentHuggingPriority(.required, for: .vertical)
        
        return label
    }()
    
    /// Replaces access to the default detailTextLabel with this cell's subtitleLabel
    public override var detailTextLabel: UILabel? {
        get {
            return subtitleLabel
        }
        set {
            if let detailTextLabel = newValue {
                subtitleLabel = detailTextLabel
                configureView()
            }
        }
    }
    
    /// Combines the accessibility labels from any cells, as needed
    public override var accessibilityLabel: String? {
        get {
            return super.accessibilityLabel ?? [descriptionLabel.text ?? "", subtitleLabel.text ?? ""].filter { !$0.isEmpty }.joined(separator: ", ")
        }
        set {
            super.accessibilityLabel = newValue
        }
    }
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        configureView()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    /// Sets up constraints and structures the views
    private func configureView() {
        selectedBackgroundView = UIView(frame: bounds)
        
        contentView.addSubview(leadingImageView)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(descriptionLabel)
        leadingImageHeightConstraint = leadingImageView.heightAnchor.constraint(equalToConstant: leadingImageSize.height)
        leadingImageWidthConstraint = leadingImageView.widthAnchor.constraint(equalToConstant: leadingImageSize.width)
        descriptionLabelLeadingConstraint = descriptionLabel.leadingAnchor.constraint(equalTo: leadingImageView.trailingAnchor, constant: 0)
        subtitleLabelLeadingConstraint = subtitleLabel.leadingAnchor.constraint(equalTo: leadingImageView.trailingAnchor, constant: 0)
        NSLayoutConstraint.activate([
            contentView.heightAnchor.constraint(greaterThanOrEqualToConstant: 60),
            leadingImageHeightConstraint,
            leadingImageWidthConstraint,
            leadingImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            leadingImageView.leadingAnchor.constraint(equalTo: contentView.readableContentGuide.leadingAnchor),
            descriptionLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            descriptionLabel.trailingAnchor.constraint(equalTo: contentView.readableContentGuide.trailingAnchor),
            descriptionLabel.bottomAnchor.constraint(equalTo: subtitleLabel.topAnchor),
            subtitleLabel.trailingAnchor.constraint(equalTo: contentView.readableContentGuide.trailingAnchor),
            subtitleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            descriptionLabelLeadingConstraint,
            subtitleLabelLeadingConstraint
        ])
        
        prepareForReuse()
    }
    
    /// Adds support for theme
    public override func prepareForReuse() {
        super.prepareForReuse()
        ThemeController.shared.$themeName
            .map { _ in ThemeController.shared.theme }
            .sink(receiveValue: { [weak self] theme in
                self?.descriptionLabel.font = theme.preferredFont(forTextStyle: .body)
                self?.descriptionLabel.textColor = theme.textColor
                self?.subtitleLabel.font = theme.preferredFont(forTextStyle: .footnote)
                self?.subtitleLabel.textColor = theme.textColorSecondary
                self?.backgroundColor = theme.backgroundColor
                self?.leadingImageView.backgroundColor = theme.backgroundColor
                self?.selectedBackgroundView?.backgroundColor = theme.accentColor
            })
            .store(in: &cancellables)
    }
    
    /// Updates the constraints with whether the image should be showing
    public override func updateConstraints() {
        leadingImageHeightConstraint.constant = leadingImageSize.height
        leadingImageWidthConstraint.constant = leadingImageSize.width
        descriptionLabelLeadingConstraint.constant = (leadingImageSize.width > 0 ? 16 : 0) + textIndent
        subtitleLabelLeadingConstraint.constant = (leadingImageSize.width > 0 ? 16 : 0) + textIndent
        
        super.updateConstraints()
    }
    
    /// Used to round the leading image, if needed
    public override func layoutSubviews() {
        leadingImageView.layer.cornerRadius = isLeadingImageRounded ? leadingImageView.bounds.width / 2 : 0
        super.layoutSubviews()
    }
}
#endif
