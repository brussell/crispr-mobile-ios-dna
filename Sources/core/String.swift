//
//  String.swift
//  CRISPR
//
//  Created by Branden Russell on 10/28/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

import Foundation

public extension String {
    /// A string that has had the whitespace and newlines removed from the leading and trailing ends. Returns nil if that leaves an empty string
    var trimmed: String? {
        let trimmed = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return trimmed.isEmpty ? nil : trimmed
    }
}

public extension Optional where Wrapped == String {
    /// Returns true if the String is nil or empty
    var isNilOrEmpty: Bool {
        return self?.isEmpty ?? true
    }
}
