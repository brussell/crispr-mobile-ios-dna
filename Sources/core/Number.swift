//
//  Number.swift
//  CRISPR
//
//  Created by Branden Russell on 10/28/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

import Foundation

public extension StringProtocol  {
    /// Returns the digits that make up the string
    var digits: [Int] {
        return compactMap{ $0.wholeNumberValue }
    }
}

public extension LosslessStringConvertible {
    var string: String { return String(self) }
}

public extension Numeric where Self: LosslessStringConvertible {
    /// Returns the digits that make up the string
    var digits: [Int] { return string.digits }
}
