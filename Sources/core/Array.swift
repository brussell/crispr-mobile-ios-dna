//
//  Array.swift
//  CRISPR
//
//  Created by Branden Russell on 10/28/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

import Foundation

public extension Array {
    /// Access element at `index`. Reading returns the element or `nil` if `index` is out of bounds. Writing only sets if `index` is in bounds.
    subscript(safe index: Int) -> Element? {
        get {
            return (indices ~= index ? self[index] : nil)
        }
        set {
            guard indices ~= index, let newValue = newValue else { return }
            self[index] = newValue
        }
    }
}
