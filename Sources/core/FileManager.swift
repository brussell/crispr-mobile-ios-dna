//
//  FileManager.swift
//  CRISPR
//
//  Created by Branden Russell on 10/28/19.
//  Copyright © 2020 Medici Ventures. All rights reserved.
//

import Foundation

public extension FileManager {
    /// The URL for the application's library directory
    var applicationLibraryDirectory: URL? {
        // We can store more permanently on iOS. We can't on tvOS
        #if os(tvOS)
        return urls(for: .cachesDirectory, in: .userDomainMask).last
        #else
        return urls(for: .libraryDirectory, in: .userDomainMask).last
        #endif
    }
    
    /// The URL for the application's document directory
    var applicationDocumentsDirectory: URL? {
        // We can store more permanently on iOS. We can't on tvOS
        #if os(tvOS)
        return URL(fileURLWithPath: NSTemporaryDirectory())
        #else
        return urls(for: .documentDirectory, in: .userDomainMask).last
        #endif
    }
    
    /// The URL for the application's download directory
    var downloadsDirectory: URL? {
        if var url = applicationLibraryDirectory?.appendingPathComponent("Downloads") {
            do {
                try createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
                var resourceValues = URLResourceValues()
                resourceValues.isExcludedFromBackup = true
                try url.setResourceValues(resourceValues)
                
                return url
            } catch {
                Logger.logWith(logLevel: .error, message: error.localizedDescription)
            }
        }
        
        return nil
    }
}
