// swift-tools-version:5.3
import PackageDescription

let package = Package(
    name: "DNA",
    platforms: [
        .iOS(.v13),
        .tvOS(.v14),
        .macOS(.v11),
        .watchOS(.v7)
    ],
    products: [
        .library(
            name: "DNA",
            targets: ["DNA"]),
    ],
    targets: [
        .target(
            name: "DNA",
            path: "Sources"
        ),
        .testTarget(
            name: "DNATests",
            dependencies: ["DNA"]),
    ]
)
